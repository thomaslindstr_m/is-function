// ---------------------------------------------------------------------------
//  is-function
//
//  function type checker
//  - variable: Variable to be checked
// ---------------------------------------------------------------------------

    function isFunction(variable) {
        return typeof variable === 'function';
    }

    module.exports = isFunction;
